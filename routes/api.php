<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'Api'], function () {
    Route::post('register', 'AuthenticateController@signup');
    Route::post('login', 'AuthenticateController@authenticate');
    Route::post('validate', 'AuthenticateController@getAuthenticatedUser');
    Route::post('verify-referral', 'AuthenticateController@verifyReferral');
});

Route::group(['namespace' => 'Auth'], function () {
    Route::post('password-reset', 'ForgotPasswordController@sendResetLinkEmail');
    Route::post('update-password', 'ResetPasswordController@reset');
});
