<?php

namespace App\Http\Controllers\Auth;

use App\BasicSetting;
use App\GeneralSetting;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    public function showLinkRequestForm()
    {
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function sendResetLinkEmail(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
               'email' => 'required|email'
            ]);

            if ($validator->fails()) {
                $response['status'] = false;
                $response['message'] = implode(",", $validator->messages()->all());
                return response()->json($response);
            }

            $user_check = User::where('email', $request->email)->first();

            if (!$user_check) {
                return response()->json([
                    'status' => false,
                    'message' => "invliad email",
                    'data' => $user_check
                ]);
            }

            $response = $this->broker()->sendResetLink(
                    $request->only('email')
                );

            if ($response === Password::RESET_LINK_SENT) {
                return response()->json([
                        'status' => true,
                        'message' => trans($response)
                    ]);
            }

            return response()->json(
                    ['email' => trans($response)]
                );
        } catch (Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage(),
            ]);
        }
    }
}
