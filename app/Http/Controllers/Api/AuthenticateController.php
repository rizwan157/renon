<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthenticateController extends Controller
{
    public function authenticate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile_no' => 'required',
            'password' => 'required'
            ]);
            
        if ($validator->fails()) {
            $response['status'] = false;
            $response['message'] = implode(",", $validator->messages()->all());
            return response()->json($response);
        }

        $field = filter_var($request->input('mobile_no'), FILTER_VALIDATE_EMAIL) ? 'email' : 'mobile_no';
        
        $request->merge([$field => $request->input('mobile_no')]);

        $credentials = $request->only($field, 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json([
                    'status' => false,
                    'message' => 'invalid credentials'
                ]);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json([
                'status' => false,
                'message' => 'Could not create token'
            ]);
        }

        // $session = Session::get();

        // all good so return the token

        $tokenUser = JWTAuth::toUser($token);
        $tokenUSer['token'] = $token;
        return response()->json([
            'status' => true,
            'message' => 'Login successful',
            'data' => [
                'token' => $token
            ]
        ]);
    }

    // somewhere in your controller
    public function getAuthenticatedUser(Request $request)
    {
        try {
            if (! $user = JWTAuth::toUser($request->token)) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }

        // the token is valid and we have found the user via the sub claim
        return response()->json(compact('user'));
    }


    public function signup(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fullname' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'mobile_no' => 'required|unique:users',
            'address' => 'required',
        ]);

        if ($validator->fails()) {
            $response['status'] = false;
            $response['message'] = implode(",", $validator->messages()->all());
            return response()->json($response);
        }

        try {
            $input = $request->input();
            $input['password'] = bcrypt($request->password);
            $input['name'] = $request->fullname;
            $user = User::create($input);

            return response()->json([
                'status' => true,
                'message' => 'Registration successful',
                'data' => [
                    'user' => $user
                ]
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function verifyReferral(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'username' => 'required|string|exists:users,username',
        ]);

        if ($validator->fails()) {
            $response['status'] = false;
            $response['message'] = implode(",", $validator->messages()->all());
            return response()->json($response);
        }

        $user = User::where('username', $request->username)->count();
        return response()->json([
            'status' => true,
            'message' => "Valid Username"
        ]);
    }

    public function getUser($id)
    {
        $userData = User::select(
            'users.id',
            'users.username',
            'users.first_name',
            'users.last_name',
            'users.name',
            'users.cnic',
            'users.phone',
            'users.landline',
            'users.email',
            'users.plan_id',
            'users.owner_id',
            'users.referBy',
            'users.gender',
            'users.dob',
            'users.bitcoin_account'
        )->where('users.id', $id)->first();

        return $userData;
    }
}
